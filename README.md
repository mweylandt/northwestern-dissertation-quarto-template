# Northwestern Dissertation Quarto Template

This is a bare-bones template for getting a quarto book project to look like a dissertation according to Northwestern's guidelines. I used this setup, and the graduate school didn't require any formatting edits. 

The compiled pdf can be found in the _book folder, or click [here](_book/Your-Dissertation-Title.pdf)

Basically all of the hard stuff -- the margins and page number placement -- are from [this latex template](https://www.overleaf.com/latex/templates/northwestern-2021-phd-template/zfrxbqzxjsny) by Martha Dunbar and Jenny Liu. Shoutout also to https://stackoverflow.com/questions/73304608/how-can-i-change-the-placement-of-table-of-contents-in-a-quarto-book-project for the workaround on getting front matter before the list of tables etc.

Other links that might prove useful:

- [Quarto templates](https://quarto.org/docs/journals/templates.html)
- [Quarto book output](https://quarto.org/docs/books/book-output.html)